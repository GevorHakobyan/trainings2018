1.5

Machine-independent languages are portable, so you can write your program once and use it on many different architectures with little or no changes. 

Machine dependent languages often have the advantage of being able to be optimized, since the compiler knows what hardware it's running on. Any program which needs to run efficiently will often benefit from being written in a machine dependent language.
