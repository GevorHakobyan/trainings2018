Fill in the blanks in each of the following statements: 
1. Which logical unit of the computer receives information from outside the computer for use by the computer? INPUT UNIT.
2. The process of instructing the computer to solve specific problems is called PROGRAMMING.
3. What type of computer language uses English-like abbreviations for machine-language instructions? ASSEMBLY LANGUAGE.
4. Which logical unit of the computer sends information that has already been processed by the computer to various devices so that the information may be used outside the computer? OUTPUT UNIT.
5. Which logical unit of the computer retains information? MEMORY UNIT.
6. Which logical unit of the computer performs calculations? ALU.
7. Which logical unit of the computer makes logical decisions? ALU.
8. The level of computer language most convenient to the programmer for writing programs quickly and easily is HIGH.
9. The only language that a computer directly understands is called that computer's MACHINE LANGUAGE.
10. Which logical unit of the computer coordinates the activities of all the other logical units? CPU.
