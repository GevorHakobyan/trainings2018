Question:
Distinguish between the terms fatal error and nonfatal error.Why might you prefer to experience a fatal error rather than a nonfatal error?
Answer:
A Fatal error is shown and the program doesn't execute, while during a nonfatal error (a logic error) the programm can still run and show no actual errors but can produce wrong results.